import React, { Component } from 'react';
import OrdersPage from './components/orderPage';

const mockOrders = [
  {
    id: 1,
    product: 'TRT-000304',
    client: 'One Company',
    status: 'In Progress'
  },
  {
    id: 2,
    product: 'APC-010001',
    client: 'The Second Company',
    status: 'In Progress',
  }
];

class App extends Component {
  render() {
    return (
      <div className="main-content">
        <OrdersPage orders={mockOrders} />
      </div>
    );
  }
}

export default App;