import React from 'react';
import Order from './order';

const OrderList = props => {
  return (
    <div className="order-list">
      <div className="order-list-title">
        <strong>{props.status}</strong>
      </div>
      {props.orders.map(order => (
        <Order key={order.id} order={order} />
      ))}
    </div>
  );
}

export default OrderList;