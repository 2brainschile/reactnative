
import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import filmsImagen from '../assets/film_image.png'
import { Appbar, Searchbar, List, Card, Paragraph, 
         Title } from 'react-native-paper';
import { Platform } from 'react-native';
import {useDispatch, useSelector} from "react-redux";
import {getFilmsAction, getFilmsFilteredAction} from '../actions/filmListActions'

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';


const MORE_ICON = Platform.OS === 'ios' ? 'dots-horizontal' : 'dots-vertical';

const DashBoard = () => {

  const dispatch = useDispatch();

  const [searchQuery, setSearchQuery] = useState('');

  let filmList= useSelector(state => state.filmList.filmListObject);

  useEffect(() => {
    dispatch(getFilmsAction())
  },[]);

  useEffect(() => {
      if(!!(filmList) && filmList.length>0){
          console.log("CTL: Llego la lista de films:");
      }
  }, [filmList]);

  const changeSearch = (value) => {
    setSearchQuery(value);
  }

  const searchList = () => {
    dispatch(getFilmsFilteredAction(searchQuery));
  }

  const renderFilms = () => {
    const filmsForRender = !!(filmList) ? filmList.map ( element => {
        return  (
          <List.Accordion
                key = {element.id}
                title={element.film} 
                left={props => <List.Icon {...props} icon="folder" />}
                >
                <Card>
                  <Title>{element.film}</Title>
                  <Paragraph>{element.leadStudio}</Paragraph>
                  <Paragraph>{element.year}</Paragraph>
                  <Card.Cover source={filmsImagen}  style={styles.image}/>
                  <Paragraph>Género: {element.genre}</Paragraph>
                  <Paragraph>Valoración audiencia %: {element.audienceScore}</Paragraph>
                  <Paragraph>Beneficios: 1,211818182</Paragraph>
                  <Paragraph>Tomates %: 43</Paragraph>
                  <Paragraph>Ingresos a nivel mundial: {element.profitability}</Paragraph>
                  <Card.Actions>
                  <Icon.Button name="edit" backgroundColor="#3b5998">
                    <Text style={{ fontSize: 15, color: '#ffffff' }}>
                      Editar los datos de la película
                    </Text>
                  </Icon.Button>
                  </Card.Actions>
                </Card>
        </List.Accordion>
      );
    }) : null;
    return filmsForRender;
}

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
            <Appbar.Header>
                <Appbar.Content title="Adictos al cine" subtitle={'La página donde encontrarás miles de películas'} />
                <Appbar.Action/>
                <Appbar.Action icon={MORE_ICON} onPress={() => {}} />
            </Appbar.Header> 
            {global.HermesInternal == null ? null : (
                <View style={styles.engine}>
                <Text style={styles.footer}>Engine: Hermes</Text>
                </View>
            )}
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
                <Searchbar
                    placeholder="Buscar"
                    onChangeText={(value) => {
                      changeSearch(value);
                    }}
                    onIconPress={searchList}
                    value={searchQuery} />
            </View>
            <View style={styles.sectionContainer}>
              <List.Section title="Lista de películas">
                {renderFilms()}
              </List.Section>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
    image : {
      width:300,
      height: 100
    },
    input: {
        margin: 15,
        height: 40,
        borderColor: '#7a42f4',
        borderWidth: 1
    },
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    engine: {
        position: 'absolute',
        right: 0,
    },
    body: {
        backgroundColor: Colors.white,
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 10,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: Colors.black,
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: Colors.dark,
    },
    highlight: {
        fontWeight: '700',
    },
    footer: {
        color: Colors.dark,
        fontSize: 12,
        fontWeight: '600',
        padding: 4,
        paddingRight: 12,
        textAlign: 'right',
    },
});

export default DashBoard;
