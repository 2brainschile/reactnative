import { createStore, applyMiddleware, compose, combineReducers  } from 'redux';
import filmList from '../reducers/filmListReducer'
import filmData from '../reducers/filmReducer'
import thunk from 'redux-thunk';

const initialState = {};

const rootReducer = combineReducers({
    filmList,
    filmData
})

const middleware = [thunk];

const store = createStore(
    rootReducer,
    initialState,
    compose(applyMiddleware(...middleware),
        typeof window ==='object' &&
                typeof window.__REDUX_DEVTOOLS_EXTENSION__ !== 'undefined' ?
                    window.__REDUX_DEVTOOLS_EXTENSION__() : f => f
) );

export default store;


