import React, { Component } from 'react';
import OrdersPage from './components/orderPage';
import { connect } from 'react-redux';

class App extends Component {
  render() {
    return (
      <div className="main-content">
        <OrdersPage orders={this.props.orders} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    orders: state.orders
  }
}

export default connect(mapStateToProps)(App);