import React from 'react';

const Order = props => {
  return (
    <div className="order">
      <div className="order-header">
        <div>{props.order.product}</div>
      </div>
      <div className="order-body">{props.order.client}</div>
      <hr />
    </div>
  );
   }

export default Order;