import React, { Component } from 'react';
import OrderList from './orderList';

const ORDER_STATUSES = ['Unstarted', 'In Progress', 'Delivered']; 

class OrderPage extends Component {
  renderOrderLists() {
    const { orders } = this.props;
    return ORDER_STATUSES.map(status => {
      const statusOrders = orders.filter(order => order.status === status);
      return <OrderList key={status} status={status} orders={statusOrders} />;
    });
  }

  render() {
    return (
      <div className="orders">
        <div className="order-lists">
          {this.renderOrderLists()}
        </div>
      </div>
    );
  }
}

export default OrderPage;