import React from 'react';
import { createStore } from 'redux'
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import orders from './reducers/orderReducer'

const store = createStore(orders);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

