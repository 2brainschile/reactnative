import React, { Component } from 'react';
import OrderList from './orderList';

const ORDER_STATUSES = ['Unstarted', 'In Progress', 'Delivered']; 

class OrderPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showNewCardForm: false,
      product: '',
      client: '',
    };
  }

  onProductChange = (e) => {
    this.setState({ product: e.target.value });
  }

  onClientChange = (e) => {
    this.setState({ client: e.target.value });
  }

  resetForm() {
    this.setState({
      showNewCardForm: false,
      product: '',
      client: '',
    });
  }

  onCreateOrder = (e) => {
    e.preventDefault();
    this.props.onCreateOrder({
      product: this.state.product,
      client: this.state.client,
    });
    this.resetForm();
  }

  toggleForm = () => {
    this.setState({ showNewCardForm: !this.state.showNewCardForm });
  }

  renderOrderLists() {
    const { orders } = this.props;
    return ORDER_STATUSES.map(status => {
      const statusOrders = orders.filter(order => order.status === status);
      return (
        <OrderList key={status} status={status} orders={statusOrders} />
      );
    });
  }

  render() {
    return (
      <div className="order-list">
        <div className="order-list-header">
          <button
            className="button button-default"
            onClick={this.toggleForm}
          >
            + Nueva orden
          </button>
        </div>
        {this.state.showNewCardForm && (
          <form className="order-list-form" onSubmit={this.onCreateOrder}>
            <input
              className="full-width-input"
              onChange={this.onProductChange}
              value={this.state.product}
              type="text"
              placeholder="producto"
            />
            <input
              className="full-width-input"
              onChange={this.onClientChange}
              value={this.state.client}
              type="text"
              placeholder="cliente"
            />
            <button
              className="button"
              type="submit"
            >
              Guardar
            </button>
          </form>
        )}

        <div className="order-lists">
          {this.renderOrderLists()}
        </div>
      </div>
    );
  }
}

export default OrderPage;