import { uniqueId } from '../actions/orderAction'

const mockOrders = [
    {
      id: uniqueId(),
      product: 'TRT-000304',
      client: 'One Company',
      status: 'In Progress'
    },
    {
      id: uniqueId(),
      product: 'APC-010001',
      client: 'The Second Company',
      status: 'In Progress',
    }
  ];


export default function orders(state = {orders : mockOrders}, action) {
    if (action.type === 'CREATE_ORDER') {
      return { orders: state.orders.concat(action.payload) };
    }

    return state
  }