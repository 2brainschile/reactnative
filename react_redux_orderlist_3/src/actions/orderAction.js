let _id = 1;
export function uniqueId() {
  return _id++;
}

export function createOrder({ product, client }) {
  return {
    type: 'CREATE_ORDER',
    payload: {
      id: uniqueId(),
      product,
      client,
      status: 'Unstarted',
    },
  };
}