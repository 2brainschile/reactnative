import React, { Component } from 'react';
import OrdersPage from './components/orderPage';
import { connect } from 'react-redux';
import { createOrder } from './actions/orderAction'

class App extends Component {

  onCreateOrder = ({ product, client }) => {
    this.props.dispatch(createOrder({ product, client }));
  }

  render() {
    return (
      <div className="main-content">
        <OrdersPage orders={this.props.orders} 
                    onCreateOrder={this.onCreateOrder}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    orders: state.orders
  }
}

//const mapState = (state) => {
//  return {
//    orders: state.orders
//  }
//}

export default connect(mapStateToProps)(App);