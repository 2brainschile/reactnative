import React from 'react';

export const MyComponentFunctional =  (props) => {

  const [componentName]= React.useState('Functional');
  const [counter,setCounter]= React.useState(1);
  //let counter = 1;
  //  function setCounter(){
  //    counter = counter +1
  //  }

  React.useEffect(() => {
    console.log("Componente funcional montado");
  }, []);

  return (
    <section>
      <h1>My Component Functional</h1>
      <p>Content in my component {componentName} {props.index}...</p>
      <p>Contador {counter}</p>          
      <button onClick={() => setCounter(counter + 1)}>Contador Funcional</button>
    </section>
  );

}




