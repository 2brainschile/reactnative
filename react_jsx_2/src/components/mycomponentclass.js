////////////////////////////////
// Creating a basic component //
////////////////////////////////
import React, { Component } from 'react';

export class MyComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      componentName: 'Class', 
      counter : 1
    }
  }

  componentDidMount() {
    console.log("Componente clase montado");
  }

  render() {
    return (
      <section>
        <h1>My Component</h1>
        <p>Content in my component {this.state.componentName} {this.props.index}...</p>
        <p>Contador {this.state.counter}</p>          
        <button onClick={() => this.setState({ counter : this.state.counter + 1})}>Contador Funcional</button>
      </section>
    );
  }
}

