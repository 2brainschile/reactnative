/////////////////////////////////////
// Describing UI structures sample //
/////////////////////////////////////
import React from 'react';
import { render } from 'react-dom';
import { MyComponent } from './components/mycomponentclass'
import { MyComponentFunctional } from './components/mycomponentfunctional'

render(
  <section>
    <header>
      <h1>A Header</h1>
    </header>
    <nav>
      <a href="item">Nav Item</a>
    </nav>
    <main>
      <MyComponent index='1'/>
      <MyComponentFunctional index='1'/>
    </main>
    <footer>
      <small>&copy; 2019</small>
    </footer>
  </section>,
  document.getElementById('root')
);
