import React, { Component } from 'react';

class NoFragmentComponent extends Component {
  render() {
    return (
      <div>
        <h1>No estoy Usand Fragments</h1>
        <p>
          Por tanto tuve que agregar un <code>div</code> extra.
        </p>
      </div>
    );
  }
}

export default NoFragmentComponent;