import React, { Component, Fragment } from 'react';

class FragmentComponent extends Component {
  render() {
    return (
      <Fragment>
        <h1>Componente con fragmentos</h1>
        <p>No tuve que agregar un <code>div</code> extra</p>
      </Fragment>
    );
  }
}

export default FragmentComponent;