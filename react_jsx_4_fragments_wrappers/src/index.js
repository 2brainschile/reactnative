import React from 'react';
import { render } from 'react-dom';
import FragmentComponent from './components/fragmentComponent'
import NoFragmentComponent from './components/noFragmentComponent'

render(
  <p>
    <FragmentComponent />
    <NoFragmentComponent />
  </p>,
  document.getElementById('root')
);

