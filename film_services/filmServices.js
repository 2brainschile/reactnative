

const express = require("express"),
app = express(),
bodyParser  = require("body-parser"),
methodOverride = require("method-override");

let filmList = require('./data.js'); 

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});


let router = express.Router();

router.get('/filmservices/:filmId', function(req, res) {
    let film = [];
    filmList.filmList.map((data) => {
        if(data.id==req.params.filmId){
            film.push(data);
        }
    });
    res.status(200).jsonp(film);	
});

router.post('/filmservices/filter', function(req, res) {
    let film = [];
    if(!!(req.body.filter)){
        filmList.filmList.map((data) => {
            if(data.genre.toUpperCase().includes(req.body.filter.toUpperCase())){
                film.push(data);
            }
        });
    }
    if(!!(req.body.filter)){
        filmList.filmList.map((data) => {
            if(data.film.toUpperCase().includes(req.body.filter.toUpperCase())){
                film.push(data);
            }
        });
    }
    res.status(200).jsonp(film);	
});

router.post('/filmservices', function(req, res) {
    let response = { error: 'bad request'}; 
    let film = {
        id : null,
        film: null,
        genre: null,
        leadStudio: null,
        audienceScore: null,
        profitability: null,
        rottenTomatoes: null,
        worldwideGross: null,
        year: 2008
    };
    if(!!(req.body)){
        film.film = req.body.film;
        film.genre = req.body.genre;
        film.leadStudio = req.body.leadStudio;
        film.audienceScore = req.body.audienceScore;
        film.profitability = req.body.profitability;
        film.rottenTomatoes = req.body.rottenTomatoes;
        film.worldwideGross = req.body.worldwideGross;
        film.year = req.body.year;
        film.rating = req.body.rating;
        if(req.body.id!==null && req.body.id!==undefined && req.body.id!==''){
            for(var i=0;i<filmList.filmList.length;i++){
                if(filmList.filmList[i].id===req.body.id)
                {
                    film.id = req.body.id;
                    filmList.filmList[i]=film;
                }
            }
        }else{
            film.id = Math.floor(Math.random() * (+6000 - +1000) + +40);
            filmList.filmList.push(film);
        }
        response = film;
    }
    res.status(200).jsonp(response);	
});



router.get('/filmservices', function(req, res) {
    res.status(200).jsonp(filmList.filmList);	
});


app.use(router);

app.listen(8010, function() {
console.log("Node server running on http://localhost:8010");
});