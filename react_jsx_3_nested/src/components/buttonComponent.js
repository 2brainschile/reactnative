import React, { Component } from 'react';

export default class ButtonComponent extends Component {
  render() {
    return <button>{this.props.children}</button>;
  }
}