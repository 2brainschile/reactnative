import React, { Component } from 'react';

export default class SectionComponent extends Component {
  render() {
    return (
      <section>
        <h2>Section</h2>
        {this.props.children} 
      </section>
    );
  }
}