
import React from 'react';
import { render } from 'react-dom';
import SectionComponent from './components/sectionComponent'
import ButtonComponent from './components/buttonComponent'

render(
  <SectionComponent>
    <ButtonComponent>Hello World</ButtonComponent>
  </SectionComponent>,
  document.getElementById('root')
);

