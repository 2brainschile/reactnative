
import React from 'react';
import { render } from 'react-dom';

render(
  <p>
    Hello World
  </p>,
  document.getElementById('root')
);


//var container = React.createElement("p", null, `Hello World`);
//render(container, document.getElementById('root'));
