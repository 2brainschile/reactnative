import React, { Component } from 'react';

export default class ImmutableButton extends Component {
  static defaultProps = {
    disabled: false,
    text: 'Soy Inmutable! Intentalo'
  };

  render() {
    const { disabled, text } = this.props;

    return <button disabled={disabled}>{text}</button>;
  }
} 