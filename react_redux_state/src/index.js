import React, {Fragment} from 'react';
import { render } from 'react-dom';
import Loader from './compoments/loader';
import ImmutableButton from './compoments/propertiesComponent';

const myComponent = render(<Fragment><Loader /><ImmutableButton /></Fragment>, document.getElementById('root'));

setTimeout(() => {
  myComponent.setState({ first: 'done!' });
}, 1000);

setTimeout(() => {
  myComponent.setState({ second: 'done!' });
}, 2000);

setTimeout(() => {
  myComponent.setState({ third: 'done!' });
}, 3000);

setTimeout(() => {
  myComponent.setState(state => ({
    ...state,
    fourth: state.doneMessage
  }));
}, 4000);
