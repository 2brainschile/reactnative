import React from 'react';
import { render } from 'react-dom';

const libros = ['La conjura de los necios', 'El cuaderno dorado', 'Nada'];

const lector = {
  Nombre: 'Antonio',
  Apellido: 'Palomo',
  Edad: 24
};


render(
  <section>
    <h1>Libros</h1>
    <ul>
      {libros.map(libro => (
        <li key={libro}>{libro}</li>
      ))}
    </ul>

    <h1>Lector</h1>
    <ul>
      {Object.keys(lector).map(atributo => (
        <li key={atributo}>
          <strong>{atributo}: </strong>
          {lector[atributo]}
        </li>
      ))}
    </ul>
  </section>,
  document.getElementById('root')
);
